package com.vodafone.exam.questions;

import com.vodafone.exam.exceptions.InputFormatException;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Scanner;

public class Question3 {
    static int countDistinctWays(int stairs) {
        if (stairs <= 1) {
            return stairs;
        }
        int totalDifferentWay = 0;
        for (int i = 1; i <= 2 && i <= stairs; i++) {
            totalDifferentWay += countDistinctWays(stairs - i);
        }
        return totalDifferentWay;
    }

    public int countDistinctWaysToClimb(String stairs) {
        validateInput(stairs);
        return countDistinctWays(Integer.parseInt(stairs) + 1);
    }

    private String takeInput() {
        Scanner in = new Scanner(System.in);
        return in.nextLine();
    }

    public void validateInput(String input) {
        if (!NumberUtils.isDigits(input) || Integer.parseInt(input) < 1) {
            throw new InputFormatException("Please enter a valid input, (input must be greater than 1 and less or equal than 1000)");
        }else if(Integer.parseInt(input) >1000){
            throw new InputFormatException("input value could not be greater than 1000");
        }
    }

    public static void main(String args[]) {
        Question3 question3 = new Question3();
        System.out.println(question3.countDistinctWaysToClimb(question3.takeInput()));
    }
}
