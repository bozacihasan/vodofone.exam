package com.vodafone.exam.questions;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Arrays;
import java.util.Optional;

public class Question1 {
    public boolean isValidCreditCardNumber(final String creditCardNumber) {
        Optional<int[]> optionalInts = copyChracterToIntArray(creditCardNumber);
        if (!optionalInts.isPresent()) {
            return false;
        }
        int[] splitedCreditCardNumber = optionalInts.get();
        for (int index = splitedCreditCardNumber.length - 2; index >= 0; index = index - 2) {
            int calculatedValue = splitedCreditCardNumber[index] * 2;
            splitedCreditCardNumber[index] = calculatedValue > 9 ? calculatedValue - 9 : calculatedValue;
        }
        return Arrays.stream(splitedCreditCardNumber).sum() % 10 == 0;
    }

    private Optional<int[]> copyChracterToIntArray(final String creditCardNumber) {
        if (StringUtils.isBlank(creditCardNumber) || !NumberUtils.isDigits(creditCardNumber)) {
            return Optional.empty();
        }
        int[] splitedCreditCardNumber = new int[creditCardNumber.length()];
        for (int index = 0; index < creditCardNumber.length(); index++) {
            splitedCreditCardNumber[index] = Character.getNumericValue(creditCardNumber.charAt(index));
        }
        return Optional.of(splitedCreditCardNumber);
    }

    public static void main(String[] args) {
        Question1 question1 = new Question1();
        String[] tests = {"6011566926687256", "370642420667124", "5417438903761250", "4518377421351212"};
        Arrays.stream(tests).forEach((test) -> {
            System.out.println(question1.isValidCreditCardNumber(test));
        });
    }
}
