package com.vodafone.exam.questions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Question2 {

    private int countOfCourse;
    private LinkedList<Integer>[] prerequisitesOfCourses;

    public void createCourseInDefaultOrder(int count) {
        this.countOfCourse = count;
        this.prerequisitesOfCourses = new LinkedList[count];
        for (int i = 0; i < count; ++i) {
            prerequisitesOfCourses[i] = new LinkedList();
        }
    }

    public void registerCouerseOfAcademicProgram(String... registerCouresePrerequisite) {
        for (String course : registerCouresePrerequisite) {
            int[] splitedCoursePrerequisite = splitEnteredCoursePrerequisite(course);
            int registeredCourse = splitedCoursePrerequisite[0];
            Arrays.stream(splitedCoursePrerequisite)
                    .filter(prerequisite -> prerequisite != registeredCourse)
                    .forEach(prerequisite -> this.addCoursePrerequisite(registeredCourse, prerequisite));
        }
    }

    private static int[] splitEnteredCoursePrerequisite(String enteredCoursePrerequisite) {
        return Arrays.stream(enteredCoursePrerequisite.split(" ")).mapToInt(Integer::parseInt).toArray();
    }

    void addCoursePrerequisite(int course, int prerequisite) {
        prerequisitesOfCourses[course].add(prerequisite);
    }

    //Topological sort algorithm
    void courseSortViaPrerequisite(int course, boolean checkedPrerequisiteOfCourse[], List orderedCourseViaPrerequisite) {
        checkedPrerequisiteOfCourse[course] = true;
        prerequisitesOfCourses[course].forEach((prerequisiteOfCourse) -> {
            if (!checkedPrerequisiteOfCourse[prerequisiteOfCourse]) {
                courseSortViaPrerequisite(prerequisiteOfCourse, checkedPrerequisiteOfCourse, orderedCourseViaPrerequisite);
            }
        });
        orderedCourseViaPrerequisite.add(course);
    }

    public int[] getSortedAcademicProgramViaPrerequisite() {
        List<Integer> orderedCourseViaPrerequisite = new ArrayList<>(countOfCourse);
        boolean checkedPrerequisiteOfCourse[] = new boolean[countOfCourse];
        for (int i = 0; i < countOfCourse; i++) {
            checkedPrerequisiteOfCourse[i] = false;
        }
        for (int i = 0; i < countOfCourse; i++) {
            if (!checkedPrerequisiteOfCourse[i]) {
                courseSortViaPrerequisite(i, checkedPrerequisiteOfCourse, orderedCourseViaPrerequisite);
            }
        }
        return orderedCourseViaPrerequisite.stream().mapToInt(Integer::intValue).toArray();
    }

    public static void main(String args[]) {
        List<Map.Entry<Integer, String[]>> testList = new ArrayList<>(4);
        testList.add(Map.entry(4, new String[]{"1 0", "2 0", "3 1 2"}));
        testList.add(Map.entry(7, new String[]{"0 1 2", "1 3", "2 3", "3 4 5", "4 6", "5 6"}));
        testList.add(Map.entry(8, new String[]{"4 0 2", "0 1 6", "2 3 7", "1 5", "6 5", "3 5", "7 5"}));
        testList.add(Map.entry(4, new String[]{"2 1 3"}));

        testList.forEach((test) -> {
            Question2 academicProgram = new Question2();
            academicProgram.createCourseInDefaultOrder(test.getKey());
            academicProgram.registerCouerseOfAcademicProgram(test.getValue());
            int[] sortedAcademicProgram = academicProgram.getSortedAcademicProgramViaPrerequisite();
            System.out.println(Arrays.toString(sortedAcademicProgram));
        });
    }
}

