import com.vodafone.exam.questions.Question2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Arrays;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Question2Test {

    private Question2 question2;

    @Before
    public void getInstanceForTestStep() {
        this.question2 = new Question2();
        System.out.println();
    }

    @Test
    public void test1() {
        System.out.println("Test 1");
        int courseCount = 4;
        question2.createCourseInDefaultOrder(courseCount);
        System.out.println(courseCount);
        String[] test = {"1 0", "2 0", "3 1 2"};
        Arrays.stream(test).forEach(System.out::println);
        question2.registerCouerseOfAcademicProgram(test);
        int[] sortedAcademicProgram = question2.getSortedAcademicProgramViaPrerequisite();
        System.out.println(Arrays.toString(sortedAcademicProgram));

        Assert.assertArrayEquals(new int[]{0, 1, 2, 3}, sortedAcademicProgram);
    }

    @Test
    public void test2() {
        System.out.println("Test 2");
        int courseCount = 7;
        question2.createCourseInDefaultOrder(courseCount);
        System.out.println(courseCount);
        String[] test = {"0 1 2", "1 3", "2 3", "3 4 5", "4 6", "5 6"};
        Arrays.stream(test).forEach(System.out::println);
        question2.registerCouerseOfAcademicProgram(test);
        int[] sortedAcademicProgram = question2.getSortedAcademicProgramViaPrerequisite();
        System.out.println(Arrays.toString(sortedAcademicProgram));

        Assert.assertArrayEquals(new int[]{6, 4, 5, 3, 1, 2, 0}, sortedAcademicProgram);
    }

    @Test
    public void test3() {
        System.out.println("Test 3");
        int courseCount = 8;
        question2.createCourseInDefaultOrder(courseCount);
        System.out.println(courseCount);
        String[] test = {"4 0 2", "0 1 6", "2 3 7", "1 5", "6 5", "3 5", "7 5"};
        Arrays.stream(test).forEach(System.out::println);
        question2.registerCouerseOfAcademicProgram(test);
        int[] sortedAcademicProgram = question2.getSortedAcademicProgramViaPrerequisite();
        System.out.println(Arrays.toString(sortedAcademicProgram));

        Assert.assertArrayEquals(new int[]{5, 1, 6, 0, 3, 7, 2, 4}, sortedAcademicProgram);
    }

    @Test
    public void test4() {
        System.out.println("Test 4");
        int courseCount = 4;
        question2.createCourseInDefaultOrder(courseCount);
        System.out.println(courseCount);
        String[] test = {"2 1 3"};
        Arrays.stream(test).forEach(System.out::println);
        question2.registerCouerseOfAcademicProgram(test);
        int[] sortedAcademicProgram = question2.getSortedAcademicProgramViaPrerequisite();
        System.out.println(Arrays.toString(sortedAcademicProgram));

        Assert.assertArrayEquals(new int[]{0, 1, 3, 2}, sortedAcademicProgram);
    }

}
