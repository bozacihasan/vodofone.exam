import com.vodafone.exam.exceptions.InputFormatException;
import com.vodafone.exam.questions.Question3;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Question3Test {

    private Question3 question3;

    @Before
    public void getInstanceForTestStep() {
        this.question3 = new Question3();
        System.out.println();
    }

    @Test
    public void test1() {
        System.out.println("Test 1");

        int differentWayToClimb = question3.countDistinctWaysToClimb("10");
        System.out.println(differentWayToClimb);
        Assert.assertEquals(89, differentWayToClimb);
    }

    @Test
    public void test2() {
        System.out.println("Test 2");
        int differentWayToClimb = question3.countDistinctWaysToClimb("20");
        System.out.println(differentWayToClimb);
        Assert.assertEquals(10946, differentWayToClimb);
    }

    @Test(expected = InputFormatException.class)
    public void test3_passedNonNumericValue() {
        System.out.println("test3_passedNonNumericValue");
        question3.countDistinctWaysToClimb("X20");
    }

    @Test(expected = InputFormatException.class)
    public void test4_passedNegatifValue() {
        System.out.println("test4_passedNegatifValue");
        question3.countDistinctWaysToClimb("-20");
    }

    @Test(expected = InputFormatException.class)
    public void test4_passedMaxValue() {
        System.out.println("test4_passedMaxValue");
        question3.countDistinctWaysToClimb("1001");
    }
}
