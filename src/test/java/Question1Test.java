import com.vodafone.exam.questions.Question1;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Question1Test {

    private Question1 question1;

    @Before
    public void getInstanceForTestStep() {
        this.question1 = new Question1();
        System.out.println();
    }

    @Test
    public void test1() {
        System.out.println("Test 1");
        String cardNumber="6011566926687256";
        System.out.println(cardNumber);
        boolean result = question1.isValidCreditCardNumber(cardNumber);
        System.out.println(result);
        Assert.assertEquals(true, result);
    }

    @Test
    public void test2() {
        System.out.println("Test 2");
        String cardNumber="370642420667124";
        System.out.println(cardNumber);
        boolean result = question1.isValidCreditCardNumber(cardNumber);
        System.out.println(result);
        Assert.assertEquals(true, result);
    }

    @Test
    public void test3() {
        System.out.println("Test 3");
        String cardNumber="5417438903761250";
        System.out.println(cardNumber);
        boolean result = question1.isValidCreditCardNumber(cardNumber);
        System.out.println(result);
        Assert.assertEquals(true, result);
    }

    @Test
    public void test4() {
        System.out.println("Test 3");
        String cardNumber="4518377421351212";
        System.out.println(cardNumber);
        boolean result = question1.isValidCreditCardNumber(cardNumber);
        System.out.println(result);
        Assert.assertEquals(false, result);
    }

    @Test
    public void test4_passedNonNumericCreditCard() {
        System.out.println("test4_passedNonNumericCreditCard");
        String cardNumber="45183774ASD21351212";
        System.out.println(cardNumber);
        boolean result = question1.isValidCreditCardNumber(cardNumber);
        System.out.println(result);
        Assert.assertEquals(false, result);
    }

    @Test
    public void test5_passedNullNumericCreditCard() {
        System.out.println("test5_passedNullNumericCreditCard");
        String cardNumber=null;
        System.out.println(cardNumber);
        boolean result = question1.isValidCreditCardNumber(cardNumber);
        System.out.println(result);
        Assert.assertEquals(false, result);
    }
    @Test
    public void test6_passedBlankNumericCreditCard() {
        System.out.println("test6_passedBlankNumericCreditCard");
        String cardNumber="";
        System.out.println(cardNumber);
        boolean result = question1.isValidCreditCardNumber(cardNumber);
        System.out.println(result);
        Assert.assertEquals(false, result);
    }
}
